# RHEL 8 Kickstarts

[![pipeline status](https://gitlab.com/kickstart-my-heart/rhel7/badges/main/pipeline.svg)](https://gitlab.com/kickstart-my-heart/rhel7/-/commits/main) 

Kickstart files for RHEL 8 and Red Hat family distributions (e.g. CentOS Stream, Rocky Linux, Alma Linux).

This repo is part of my [Kickstart My Heart](https://gitlab.com/kickstart-my-heart) project, where I maintain automated installations for a variety of operating systems.

## Links
* **Main Repo:** https://gitlab.com/kickstart-my-heart/rhel7

### System requirements
* Requires a minimum of `64GB` disk space for a `headless` install.

### Configuring build options
To configure build options, copy the default `config.yml`:
```sh
cp -fv files/default/config.yml files/override/config.yml
```
Inside the new `files/override/config.yml` file, you can make changes to the default `kickstart_vars` variable, which defines what Kickstart files will be built when running this playbook. The variable in this file will take priority over the variables in the default `config.yml` file.

This playbook uses [vaulted variables](https://docs.ansible.com/ansible/latest/user_guide/vault.html) to store passwords, mostly as a demonstration of one way this can be done. The vault password for the default passwords is `ansible`. You will be asked for this when running the `make` commands below.

### Compiling Kickstart config files and ISOs locally
Make any necessary changes to the `ks.cfg.j2` or files in the `override/` directory and run the below commands.

Enter the build environment (you must have Podman installed)
```sh
make
```

Inside the build environment:
```sh
make kickstart
```

### Booting this install (provisioning a 'Golden Master' VM image with Packer)
Inside the environment:
```sh
make packerconf
```

Now, back on the host, enter into the relevant `packer/` directory for your virtualisation platform.

And then initialise Packer and build the image (Note that I use the full path to the Packer binary in the Makefile because of [this bug](https://github.com/hashicorp/packer/issues/11081)):
```sh
make build
```

To clear previously-built files from the current directory (this works in both the root directory and in the Packer subdirectories):
```sh
make clean
```

### Booting this install (installing directly to physical hardware, or provisioning a VM without using Packer)
Make local bootable Kickstart `OEMDRV` ISOs that will autoinstall RHEL for us:
```sh
make kickstart
```

Next steps:
* Attach RHEL 8 ISO to disk bay 1.
* Attach this Kickstart `OEMDRV` ISO to bay 2.
* Boot host.
* Installation should start automatically.

Respin a full RHEL 8 ISO into a bootable ISO with Kickstart included (good for physical installs over BMC where only one ISO can be provided):
```sh
make iso
```

### Post-install steps
```sh
# Register to RHSM
sudo subscription-manager register

# Change the password for the root account
sudo passwd root

# Change the password for the user account
sudo passwd user

# Change the active LUKS encryption key used for the installed system
sudo cryptsetup luksChangeKey /dev/sda3
```

### Post-install remediation for CIS hardened systems
```sh
# Create an Ansible inventory file and add your host(s)
nano inventory

# Run the Ansible playbook to remediate the installed hosts
ansible-playbook -i inventory --ask-pass --ask-become-pass --ssh-extra-args '-o PreferredAuthentications=password -o PubkeyAuthentication=no -o StrictHostKeyChecking=no' cis_postinstall.yml
```

### Config Options
Config options which can be provided in `config.yml`:

* `active_directory`
  * Boolean
  * Default: `false`
  * Controls whether all the necessary dependencies for joining the server to Active Directory will be installed.
* `cis_hardening`
  * Boolean
  * Default: `false`
  * Controls whether the CIS hardening benchmark will be applied to the server.
* `disable_cis_timeouts`
  * Boolean
  * Default: `false`
  * Disables SSH and shell timeouts if CIS hardening is used and this is desired.
* `disk_encryption`
  * Boolean
  * Default: `false`
  * Defines whether LUKS encryption will be used for the primary disk.
* `disk_path`
  * String
  * Default: *No default.*
  * This variable has no default and must be specified for every entry in the `config.yml` file. This controls the path that the installer will use to decide which disk to write to. It is recommended to use a `/dev/disk/by-id/xxxxx` style path where possible, to avoid situations where the installer overwrites the wrong disk. For fresh virtual machines or image deployments it is generally safe to use something like `/dev/sda`.
* `enable_loading_kernel_modules`
  * Boolean
  * Default: `false`
  * Defines whether or not we set the sysctl that disables loading any kernel modules. This only takes effect if `cis_hardening` is also enabled.
* `grow_home`
  * Boolean
  * Default: `false`
  * Controls whether the `/home` logical volume will grow to fill all remaining space on the LV Group. By default this does not happen to provide maximum flexibility on an installed system.
* `hostname`
  * String (`rhel7`, `localhost`, `any-string`, etc.)
  * Default: `rhel7`
  * Controls what hostname the deployed system will use.
* `network_interface`
  * String (`link`, `eno1`, `ens192`, etc.)
  * Default: `link`
  * Controls which network interface the network configuration will be applied to. You may want to statically define this if you want to deploy to a system with multiple active interfaces. Otherwise the default is `link`, which will apply the configuration to the first active ethernet interface.
* `network_type`
  * String (`none`, `static`, `dhcp`)
  * Default: `none`
  * Controls what kind of network setup the deployed machine will use. By default no network is configured. This is to allow for maximum flexibility when deployed.
* `ntp_servers`
  * String (`10.10.10.1`, `time.cloudflare.com`, etc.)
  * Default: *No Default.*
  * Controls what NTP servers will be passed to the `timezone` command in the Kickstart file. If none are provided, NTP will not be configured.
* `oscap_user`
  * Boolean
  * Default: `false`
  * Controls whether to add the `oscap-user` user to the sudoers file with a set of restricted permissions for running OpenSCAP validation scans against the host when deployed. This requires a corresponding `oscap-user` entry in the `create_users.j2` file. Please see the default file for an example.
* `packer_disk_gb`
  * Integer
  * Default: `64`
  * Specifies the disk size in gigabytes that Packer should use when provisioning a machine. This defaults to `64` and should not be lower than this value but can be set higher.
* `poweroff`
  * Boolean
  * Default: `false`
  * Controls whether the Kickstart file instructs the machine to power off at the end of the Kickstart run. This is useful for Packer or image-based installs. The default is to reboot the VM and eject the Kickstart ISO.
* `remove_machine_id`
  * Boolean
  * Default: `false`
  * Controls whether `/etc/machine-id` gets removed after the installation is complete. This is mostly useful if creating an image which will be deployed as a template to a large number of servers.
* `timezone`
  * String (`Etc/UTC`, `Europe/London`, etc.)
  * Default: `Etc/UTC`
  * Configure the timezone if desired. Defaults to UTC.
* `vmware_vhw_ver`
  * String
  * Default: `13`
  * Configure the [Virtual Hardware version](https://kb.vmware.com/s/article/1003746) of the VMware VMs that Packer will deploy. A value of `13` provides compatibility with VMware 6.5 and includes support for EFI and Secure Boot. Higher values may provide more recently added features.

### Changelog
* `7.9.1` - Initial RHEL 7 release, based around RHEL 7.9.
