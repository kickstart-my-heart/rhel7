datacenter_name = "datacenter"
datastore_name  = "vm_datastore_name"
iso_paths = [
  "[iso_datastore_name] ISOs/rhel-8.4-x86_64-dvd.iso",
]
network_name     = "VM Network"
vcenter_endpoint = "vm.localhost"
vcenter_host     = "vcenter.localdomain"
vcenter_password = "greatpassword"
vcenter_user     = "user"