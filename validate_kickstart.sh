#!/bin/sh
set -xe

# List currently supported RHEL versions we want to
# validate our Kickstart against here:
for ver in RHEL7; do
  echo "[$(date)] Validating ${ver} Kickstart file at: ${1}" | tee --append debug.log
  ksvalidator --version ${ver} "${1}" | tee --append debug.log
  ksflatten --version ${ver} --config ${1}  # Doesn't output to file but will fail with a non-zero RC if something is wrong
done
